import { Loading, QSpinnerBars } from "quasar";
export const setUserName = (state, user) => {
    state.userName = user;
};
export const setUserEmail = (state, email) => {
    state.userEmail = email;
};
export const logout = (state) => {
    state.userEmail = null;
    state.userName = null;
};