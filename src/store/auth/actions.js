import API from "../../config";
export const sendLoginRequest = async({ commit }, data) => {
    await API.login("loginuser", data)
        .then(response => {
            if (response.state == "error") {
                sessionStorage.setItem("login_status", response.detail);
            } else {
                sessionStorage.setItem("login_status", response.detail);
                sessionStorage.setItem("user_name", response.data[0].name);
                sessionStorage.setItem("user_email", response.data[0].email);
                commit("setUserName", response.data[0].name);
                commit("setUserEmail", response.data[0].email);
            }

        })
        .catch(error => {
            console.log(error);
        });
}
