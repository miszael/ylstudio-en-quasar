import API from "../../config";
export const fetchClientes = async({ commit }) => {
    await API.get("getclientes")
        .then(response => {
            commit("setClientes", response.data);
        })
        .catch(error => {
            console.error(error);
        });
};
export const postClientes = async({ commit }, data) => {
    await API.post_params("postcliente", data)
        .then(response => {
            commit("setClientes", response.data);
        })
        .catch(error => {
            console.error(error);
        });
};
export const editClientes = async({ commit }, data) => {
    await API.post_params("putcliente", data)
        .then(response => {
            commit("setClientes", response.data);
        })
        .catch(error => {
            console.error(error);
        });
};
export const deleteCliente = async({ commit }, data) => {
    await API.delete("deletecliente", data)
        .then(response => {
            // commit("setClientes", response.data);
            console.log(response.detail);
        })
        .catch(error => {
            console.log(error);
        });
};
