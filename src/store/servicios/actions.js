import API from "../../config";
export const fetchServicios = async({ commit }) => {
    await API.get("getservicios")
        .then(response => {
            commit("setServicios", response.data);
        })
        .catch(error => {
            console.error(error);
        });
};