import Vue from 'vue'
import Vuex from 'vuex'
import auth from './auth'
import citas from './citas'
import clientes from './clientes'
import codigos from './codigos'
import horarios from './horarios'
import principal from './principal'
import promociones from './promociones'
import servicios from './servicios'
import ubicaciones from './ubicaciones'
import usuarios from './usuarios'

// import example from './module-example'

Vue.use(Vuex)

/*
 * If not building with SSR mode, you can
 * directly export the Store instantiation;
 *
 * The function below can be async too; either use
 * async/await or return a Promise which resolves
 * with the Store instance.
 */

export default function( /* { ssrContext } */ ) {
    const Store = new Vuex.Store({
        modules: {
            auth,
            citas,
            clientes,
            codigos,
            horarios,
            principal,
            promociones,
            servicios,
            ubicaciones,
            usuarios
        },

        // enable strict mode (adds overhead!)
        // for dev mode only
        strict: process.env.DEBUGGING
    })

    return Store
}