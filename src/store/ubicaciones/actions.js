import API from "../../config";
export const fetchUbicaciones = async({ commit }) => {
    await API.get("getubicaciones")
        .then(response => {
            commit("setUbicaciones", response.data);
        })
        .catch(error => {
            console.error(error);
        });
};