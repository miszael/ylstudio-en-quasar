import API from "../../config";
export const fetchHorarios = async({ commit }) => {
    await API.get("gethorarios")
        .then(response => {
            commit("setHorarios", response.data);
        })
        .catch(error => {
            console.error(error);
        });
};
export const fetchHorDisp = async({ commit }) => {
    await API.get("gethorariosdisponibles")
        .then(response => {
            commit("setHorariosDisponibles", response.data);
        })
        .catch(error => {
            console.error(error);
        });
};
export const updateFecha = async({ commit }, data) => {
    await API.post_params("updatefecha", data)
        .then(response => {
            // commit("setHorarios", response.data);
            console.log(response.data.detail);
        })
        .catch(error => {
            console.error(error);
        });
};
