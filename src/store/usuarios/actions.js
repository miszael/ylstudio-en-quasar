import API from "../../config";
export const fetchUsuarios = async({ commit }) => {
    await API.get("getusuarios")
        .then(response => {
            commit("setUsuarios", response.data);
        })
        .catch(error => {
            console.error(error);
        });
};