import API from "../../config";
export const fetchCodigos = async({ commit }) => {
    await API.get("getcodigos")
        .then(response => {
            commit("setCodigos", response.data);
        })
        .catch(error => {
            console.error(error);
        });
};
export const postCodigos = async({ commit }, data) => {
    await API.post_params("postcodigo", data)
        .then(response => {
            commit("setCodigos", response.data);
        })
        .catch(error => {
            console.error(error);
        });
};
export const editCodigos = async({ commit }, data) => {
    await API.post_params("putcodigos", data)
        .then(response => {
            commit("setCodigos", response.data);
        })
        .catch(error => {
            console.error(error);
        });
};
export const deleteCodigo = async({ commit }, data) => {
    await API.delete("deletecodigos", data)
        .then(response => {
            // commit("setCodigos", response.data);
            console.log(response.detail);
        })
        .catch(error => {
            console.log(error);
        });
};
