import API from "../../config";
export const fetchPromociones = async({ commit }) => {
    await API.get("getpromos")
        .then(response => {
            commit("setPromociones", response.data);
        })
        .catch(error => {
            console.error(error);
        });
};