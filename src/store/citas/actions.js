import API from "../../config";
export const fetchCitas = async({ commit }) => {
    await API.get("getcitasadmin")
        .then(response => {
            commit("setCitas", response.data);
        })
        .catch((error) => {
            console.error(error)
        })
};
export const fetchCitasNow = async({ commit }) => {
    await API.get("getcitasadminnow")
        .then(response => {
            commit("setCitasNow", response.data);
        })
        .catch(error => {
            console.error(error);
        });
};
export const postCitas = async({ commit }, data) => {
    await API.post_params("postcita", data)
        .then(response => {
            commit("setCitas", response.data);
        })
        .catch(error => {
            console.error(error);
        });
};
export const editCitas = async({ commit }, data) => {
    await API.post_params("putcita", data)
        .then(response => {
            commit("setCitas", response.data);
        })
        .catch(error => {
            console.error(error);
        });
};
export const deleteCita = async({ commit }, data) => {
    await API.delete("deletecita", data)
        .then((response) => {
            // commit("setCitas", response.data);
            console.log(response.detail)
        })
        .catch(error => {
            console.log(error);
        });
};
export const changeState = async({ commit }, data) => {
    await API.post_params("changestate", data)
        .then((response) => {
            console.log(response.detail);
        })
        .catch(error => {
            console.log(error);
        })
};
