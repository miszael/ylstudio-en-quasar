const routes = [{
        path: "/auth",
        component: () =>
            import ("layouts/MainLayout.vue"),
        children: [{
                path: "index",
                component: () =>
                    import ("pages/Index.vue")
            },
            {
                path: "principal",
                component: () =>
                    import ("components/Principal.vue")
            },
            {
                path: "citas",
                component: () =>
                    import ("components/Citas.vue")
            },
            {
                path: "clientes",
                component: () =>
                    import ("components/Clientes.vue")
            },
            {
                path: "codigos",
                component: () =>
                    import ("components/Codigos.vue")
            },
            {
                path: "horarios",
                component: () =>
                    import ("components/Horarios.vue")
            },
            {
                path: "horariosvencidos",
                component: () =>
                    import ("components/HorariosVencidos.vue")
            },
            {
                path: "promociones",
                component: () =>
                    import ("components/promociones.vue")
            },
            {
                path: "servicios",
                component: () =>
                    import ("components/Servicios.vue")
            },
            {
                path: "ubicaciones",
                component: () =>
                    import ("components/Ubicaciones.vue")
            },
            {
                path: "usuarios",
                component: () =>
                    import ("components/Usuarios.vue")
            }
        ],
    },
    {
        path: "/",
        component: () =>
            import ("pages/login.vue")
    },
    {
        path: "*",
        component: () =>
            import ("pages/Error404.vue")
    }
];

export default routes;