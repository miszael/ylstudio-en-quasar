import axios from "axios";
import { Loading, Notify, QSpinnerBars, QSpinnerDots } from "quasar";
localStorage.setItem("url", "https://ylstudio.com.mx/api/"); //Server
// localStorage.setItem("url", "http://192.168.1.73/ylstudiopublic/public/api/"); //Local movil
var server = localStorage.getItem("url");
// var headers = { Authorization: 'Token ' + localStorage.getItem("token") };
var headers = {};
export default {
    server_url() {
        return server;
    },
    get(url) {
        if (sessionStorage.getItem("user_name") != '') {
            Loading.show({
                spinner: QSpinnerDots,
                spinnerColor: "blue",
                message: "Recuperando datos"
            });
            return axios
                .get(server + url, {
                    headers
                })
                .then(response => {
                    Loading.hide();
                    return response.data;
                })
                .catch(error => {
                    Loading.hide();
                    if (error == "Error: Network Error") {
                        Notify.create({
                            type: "negative",
                            message: "No se pudo conectar con el servidor, revisa tu conexión a internet o intentalo de nuevo mas tarde"
                        });
                    } else {
                        Notify.create({
                            type: "negative",
                            message: "El servidor envió una respuesta inválida"
                        });
                    }
                    return error;
                });
        } else {
            Notify.create({
                type: "negative",
                message: "Inicia sesión primero"
            });
        }

    },
    post(url) {
        return axios
            .post(server + url, {
                headers
            })
            .then(response => {
                return response.data;
            })
            .catch(error => {
                if (error == "Error: Network Error") {
                    Notify.create({
                        type: "negative",
                        message: "No se pudo conectar con el servidor, revisa tu conexión a internet o intentalo de nuevo mas tarde"
                    });
                } else {
                    Notify.create({
                        type: "negative",
                        message: "El servidor envió una respuesta inválida"
                    });
                }
                return error;
            });
    },
    post_params(url, formData) {
        Loading.show({
            spinner: QSpinnerDots,
            spinnerColor: "blue",
            message: "Recuperando datos"
        });
        return axios
            .post(server + url, formData, {
                headers: { "Content-Type": "multipart/form-data" }
            })
            .then(response => {
                Loading.hide();
                return response;
            })
            .catch(function(error) {
                Loading.hide();
                if (error == "Error: Network Error") {
                    Notify.create({
                        type: "negative",
                        message: "No se pudo conectar con el servidor, revisa tu conexión a internet o intentalo de nuevo mas tarde"
                    });
                } else {
                    Notify.create({
                        type: "negative",
                        message: "El servidor envió una respuesta inválida"
                    });
                }
                return error;
            });
    },
    login(url, params) {
        Loading.show({
            spinner: QSpinnerBars,
            spinnerColor: "green",
            message: "Iniciando sesión"
        });
        return axios
            .post(server + url, params, {
                headers: { "Content-Type": "multipart/form-data" }
            })
            .then(response => {
                Loading.hide();
                if (response.data.state == "error") {
                    Notify.create({
                        type: "negative",
                        message: response.data.detail
                    });
                }
                return response.data;
            })
            .catch(function(error) {
                Loading.hide();
                if (error == "Error: Network Error") {
                    Notify.create({
                        type: "negative",
                        message: "No se pudo conectar con el servidor, revisa tu conexión a internet o intentalo de nuevo mas tarde"
                    });
                } else {
                    Notify.create({
                        type: "negative",
                        message: "El servidor envió una respuesta inválida"
                    });
                }
                return error;
            });
    },
    delete(url, params) {
        Loading.show({
            spinner: QSpinnerBars,
            spinnerColor: "negative",
            message: "Eliminando"
        });
        return axios
            .post(server + url, params, {
                headers: { "Content-Type": "multipart/form-data" }
            })
            .then(response => {
                Loading.hide();
                if (response.data.state == "error" || response.data.data === 0) {
                    Notify.create({
                        type: "negative",
                        message: "Error al eliminar"
                    });
                } else {
                    Notify.create({
                        type: "positive",
                        message: "Eliminación exitosa"
                    });
                }
                return response.data;
            })
            .catch(function(error) {
                Loading.hide();
                if (error == "Error: Network Error") {
                    Notify.create({
                        type: "negative",
                        message: "No se pudo conectar con el servidor, revisa tu conexión a internet o intentalo de nuevo mas tarde"
                    });
                } else {
                    Notify.create({
                        type: "negative",
                        message: "El servidor envió una respuesta inválida"
                    });
                }
                return error;
            });
    }
};